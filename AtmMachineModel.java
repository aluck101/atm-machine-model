/* This program is model of an atm machine work in a command line code
 * by Tunde Aluko
 */


import java.util.Scanner;

public class AtmMachineModel {
	private int availBalance = 10000;
	private int totalBalance = 15000;
	Scanner input = new Scanner(System.in);
	
	
	
public void startUp() {
	userName();
	userPin();
	}
	
public void userPin() //This method is use to check and validates the pin
   {
	System.out.println("Enter your 4-digits secret number: ");
	boolean response = false;
	String pin = "1111";
	if(!response) {
	for(int i = 0; i < 3; i++) {
		if(pin.equals(input.next().trim())) {
			mainMenu();
			break;
			} else { System.out.println("Invalid Pin, try again: ");
				}
		     if (i == 2) {
		    	 System.out.println("Card Blocked");
		    	 exit();
		     }
			}
		}
		}
	
	
		
public String userName() {
	String name = "Tunde";
	System.out.println("Welcome Tunde");
	return name;	
	}
	
public void mainMenu() {
	System.out.println("Main Menu");
	System.out.println("[1] Check Your Balance");
	System.out.println("[2] Withdraw");
	System.out.println("[3] Exit");
	int choice = input.nextInt();
	
	switch(choice) {
	
	case 1:
	userBalance();
	break;
	
	case 2:
	withdraw();
	break;
	
	case 3: 
	exit();
	break;
	
	default: 
	System.out.println("Invalid option, Transaction cancelled");
		
	}
	
	}
	
	public void userBalance() {
		System.out.println("Your total balance is " + "N"+ totalBalance);
		System.out.println("Your available balance is " + "N" +availBalance);
		mainMenu();		
		}
	
	
		
	
	public void exit() {
		System.out.println("Exiting out of program, Goodbye");
		
	}

	public void checkWithdraw(int withdrawAmount) {
		if(withdrawAmount > availBalance) {
			System.out.println("Insufficient Balance!");
		} else {
			availBalance -= withdrawAmount;
			totalBalance -= withdrawAmount;
			System.out.println("Take you Cash");
		}
	}


	public void withdraw() {
		System.out.println("[1] N1000");
		System.out.println("[2] N2000");
		System.out.println("[3] N5000");
		System.out.println("[4] N10000");
		System.out.println("[5] N20000");
		System.out.println("[6] Back to Main Menu");
		System.out.println("Choice: ");
		int withdrawChoice;
		withdrawChoice = input.nextInt();
	
	switch(withdrawChoice) {
		case 1:
		checkWithdraw(1000);
		mainMenu();
		break;
		
		case 2:
		checkWithdraw(2000);
		mainMenu();
		break;
		
		case 3:
		checkWithdraw(5000);
		mainMenu();
		break;
		
		case 4: 
		checkWithdraw(10000);
		mainMenu();
		break;
		
		case 5:
		checkWithdraw(20000);
		mainMenu();
		
		case 6:
		mainMenu();
		
		default: 
			System.out.println("Invalid option, Transaction cancelled");
			exit();
	}
		
	}	
	public static void main(String args[]) {
		AtmMachineModel atm = new AtmMachineModel();
		atm.startUp();
	}
	
	}
